-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2018 at 06:36 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portfolio_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `name` varchar(100) NOT NULL,
  `designation` varchar(250) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `phone` int(15) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` text NOT NULL,
  `profile_picture` varchar(200) NOT NULL,
  `cover_picture` varchar(200) NOT NULL,
  `about_me` text NOT NULL,
  `signature_image` varchar(200) NOT NULL,
  `cover_about` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`name`, `designation`, `dob`, `phone`, `address`, `email`, `website`, `profile_picture`, `cover_picture`, `about_me`, `signature_image`, `cover_about`) VALUES
('utsav', 'Website Developer, Software Developer', '1995-09-30', 2147483647, 'kirtipur, Kathmandu', 'vastu.bhandari45@gmail.com', 'utsavbhandari.com', '../images/pp.jpg', '../images/20160511_132536.jpg', '                                                                                                        I am a freelance web developer.I have serious passion for UI effects, animations and creating intuitive, dynamic user experiences. I also love doing the back end.I have worked for two companies (Cuidado Technology Pvt. Ltd and Innovative Creation Services Pvt. Ltd.) in the field of software engineering and graphic designing. I have also worked for a Turkish company on their project called SMART HOME. My academic and work experience up to this point has given me an extensive variety of down to earth information and aptitudes that will be immensely helpful when working for my clients. In addition to my specialized knowledge of information technology, I have an appreciation for the breadth of this field and the nearly endless possibilities for further study. In a competitive environment of technology, there are many things that I can offer such as flexibility, enthusiasm and commitment. I have confidence to work independently at the same time has the enjoyment to work on projects.                                                             \r\n                                                                                                                                                        ', '../images/Capture.PNG', '                                                                                                                                                                                                                                                                                                                  ');

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `description` text NOT NULL,
  `address` text NOT NULL,
  `phone1` varchar(100) NOT NULL,
  `phone2` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `support` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`description`, `address`, `phone1`, `phone2`, `email`, `support`, `facebook`, `linkedin`, `instagram`) VALUES
('I am here to answer all your questions regarding my experiences...\r\n                                                        ', 'kirtipur, Kathmandu', '+977-9843060605', '+977-9843060605', 'vastu.bhandari45@gmail.com', 'vastu.bhandari45@gmail.com', 'https://www.facebook.com/vastu45', 'https://www.linkedin.com/in/utsav-bhandari-5a704b104/', 'https://www.instagram.com/vastu.utsav/');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `education_id` int(10) NOT NULL,
  `level` varchar(100) NOT NULL,
  `start_end_date` varchar(100) NOT NULL,
  `course` varchar(100) NOT NULL,
  `course_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`education_id`, `level`, `start_end_date`, `course`, `course_description`) VALUES
(4, 'Bachelors', '2015-2018', 'Bsc (Hons) Computing', 'The degree programme is comprised of a one year Foundation Year provided by The British College followed by a three year degree validated by Leeds Beckett University.                                                            \r\n                                                        '),
(5, 'Plus Two (+2)', '2012-2014', 'Science', ' The primary goal of it is to provide advanced and pragmatic in-depth knowledge of science to produce high caliber science scholars. It aims to create foundation for future doctors, engineers, agriculturist, forest officials and scientist.                                                            \r\n                                                        '),
(6, 'School Leaving Certificate (SLC)', '2012', 'SLC', '    The School Leaving Certificate , popularly abbreviated as SLC is the final examination in the secondary school systemof Nepal. It is equivalent to GCSE, the academic qualification in England                                                         \r\n                                                        ');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `experience_id` int(10) NOT NULL,
  `start_end_date` varchar(100) NOT NULL,
  `workplace` varchar(200) NOT NULL,
  `post` varchar(200) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`experience_id`, `start_end_date`, `workplace`, `post`, `description`) VALUES
(1, '2017', 'Parkyeri (Istanbul, Turkey)', 'Intern', 'Worked as an intern for Smart Home Project                                                          \r\n                                                        '),
(2, '2016', 'Cuidado Technology', 'Back End Developer', '   Worked as Graphic Designer and Back End Developer           '),
(3, '2015 ', ' Innovative creation Services ', 'Web Developer', 'Developed ICS  (Innovative creation Services)  Website                                            \r\n                                                        ');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `name`, `email`, `company_name`, `phone`, `message`) VALUES
(1, 'utsav bhandari', 'vastu.bhandari45@gmail.com', 'vastu', '9843060605', 'Hi this is a test'),
(2, 'utsav bhandari', 'vastu.bhandari45@gmail.com', 'vastu', '9843060605', 'Hi this is a test 2');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `portfolio_id` int(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `tag` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `category` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `skills_used` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `title`, `description`, `tag`, `image`, `category`, `link`, `skills_used`) VALUES
(3, 'Online News Website', 'This is a responsive online news website. This website was designed on 2017 for my college project. It provides facilities to select, browse or search a subset of this data.This website consists of many features like form validations, captcha, server-side security features such as filtering and sanitization of any data input, in-memory data structure and functionality to manage the collection of objects,XML and JSON document extraction of data from the database.\r\nThis website also has a admin panel where the admin can view, update and delete all the data.\r\nThis website is likely to be more secured because of use of Captcha Validation.\r\n                                                                                                                    \r\n                                                        ', 'Motive News', '../images/img1.jpg', 'Web', 'https://motivenews.000webhostapp.com/', 'HTML, CSS, JavaScript, JQuery, Ajax, Angular Js, Bootstrap, PHP, XML/XSLT                                                            \r\n                                                        '),
(7, 'Forum Website', 'This is a forum website, a simple Model, View, Controller (MVC) application built using Laravel Framework . The users can post threats on any programming errors and problems and also other clients can suggest a good comment to a particular thread. This website was designed on 2017 for my college project. The general purpose to create this application is to understand the MVC Framework practically like Addding functionality to existing controllers/views, Adding functionality via other controllers, models and views, Including authentication and authorisation, etc. MVC framework for the web development provides Faster development process, Ability to provide multiple views, Support for asynchronous technique.                                                             \r\n                                                        ', 'Canvass Forum', '../images/canvass.jpg', 'Web', 'https://canvassforum.000webhostapp.com/', 'Laravel                                                            \r\n                                                        '),
(8, 'ICS Website', 'This is a website designed for a Consultancy  that helps students study abroad. This website provides information about the study destinations, universities, test preparation courses and how to apply abroad. The users are able to post their required documents through the website to    apply for the visa. For user safety, I have used password encryption as a security measure. This website is likely to be more faster as I have reduced results page loading time by 30% by optimizing SQL queries.                   ', 'Innovative Creation Services', '../images/ics.jpg', 'Web', 'https://icsnepal.000webhostapp.com/', 'HTML, CSS, JavaScript, JQuery, PHP,MySql'),
(9, 'Visit Britain Tourism Website', ' This is a website designed for a tourists who wants to visit to the UK. This website was designed on 2016 for my college project. This website provides the detail description of the famous places in the UK, news and events happening in different places. This website has provide features like an interface to allow customers to view the points of interest, to allow users to select points of interest to temporarily store in a favorites collection, A facility to allow users to register and login and A facility to allow authenticated admin users to manage the points of interest and news and events presented by the system. The website is well validated for user register and login and it uses server-side security features such as filtering and sanitization of any data input. It also uses in-memory data structure and functionality to manage the collection of objects. This website is likely to be more faster as I have reduced results page loading time by 30% by optimising SQL queries. I have used some other features like Password encryption for user security.                                 \r\n                                                        ', 'Visit Britain', '../images/britain.jpg', 'Web', 'https://visitbritain.000webhostapp.com/index.php', ' HTML, CSS, JavaScript, JQuery, PHP , MySql                                                           \r\n                                                        '),
(10, 'Vastu Tech', 'This is a static website. It gives the information about the latest, popular devices in the market.  This website is based on Bootstrap Framework.This is my first project in web development.This website is a demonstration of the Front End Design.', 'Vastu Tech Info', '../images/vastu.jpg', 'Web', 'https://vastutech.000webhostapp.com/', 'HTML, CSS, JQuery, JavaScript and Photoshop');

-- --------------------------------------------------------

--
-- Table structure for table `resume`
--

CREATE TABLE `resume` (
  `resume_id` int(10) NOT NULL,
  `resume_title` varchar(200) NOT NULL,
  `resume_file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resume`
--

INSERT INTO `resume` (`resume_id`, `resume_title`, `resume_file`) VALUES
(1, '', '../documents/resume.pdf'),
(2, '', '../documents/Cover letter.docx'),
(3, '', '../documents/resume.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `skill_id` int(10) NOT NULL,
  `skills_on` varchar(200) NOT NULL,
  `skills_desc` text NOT NULL,
  `picture` varchar(200) NOT NULL,
  `skill_level` varchar(10) NOT NULL,
  `skill_bar_color` varchar(100) NOT NULL,
  `colorback` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`skill_id`, `skills_on`, `skills_desc`, `picture`, `skill_level`, `skill_bar_color`, `colorback`) VALUES
(5, 'HTML', '                                                            \r\n                                                        ', '../images/s1.jpg', '85%', '#81DAF5', '#2ECCFA'),
(6, 'CSS', '                                                            \r\n                                                        ', '../images/s2.jpg', '78%', '#51a488', '#5bb899'),
(7, 'JavaScript', '                                                            \r\n                                                        ', '../images/s3.jpg', '50%', '#5b6bb4', '#7798ff'),
(9, 'JQuery', '                                                            \r\n                                                        ', '../images/s4.jpg', '45%', '#cc6c1d', '#e37921'),
(10, 'PHP', '                                                            \r\n                                                        ', '../images/', '75%', '#998741', ' #aa9749'),
(11, 'AngularJS', '                                                            \r\n                                                        ', '../images/', '35%', '#e5d210', '#ffea12'),
(12, 'WordPress', '                                                            \r\n                                                        ', '../images/', '75%', '#905b5b', '#a16666');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `username`, `password`) VALUES
(1, 'vastu.bhandari45@gmail.com', 'vastu', 'vastu.bhandari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`education_id`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`experience_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `resume`
--
ALTER TABLE `resume`
  ADD PRIMARY KEY (`resume_id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `education_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `experience_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `portfolio_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `resume`
--
ALTER TABLE `resume`
  MODIFY `resume_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `skill_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
