<?php
		 
	require_once 'connection.php';
	
	if (isset($_REQUEST['id'])) {
			
		$id = intval($_REQUEST['id']);
		$sql = "SELECT title,category,description,skills_used,link,image,tag,evidence1,evidence2,evidence3,evidence4,evidence5 FROM portfolio WHERE portfolio_id='$id' ";
		$result = mysqli_query($conn, $sql);

								                if (mysqli_num_rows($result) > 0) {
								                    // output data of each row
								                    while($row = mysqli_fetch_assoc($result)) {
								                       ?>
			
		
			
		<div class="table-responsive">
		
		<table class="table table-striped table-bordered">

			<div id="myCarousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#myCarousel" data-slide-to="1"></li>
			    <li data-target="#myCarousel" data-slide-to="2"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner">
			    <div class="item active">
			            <img src="images/<?php echo $row['image']; ?>" style="width:100%;">
			            <div class="carousel-caption">
			              
			              
			            </div>
			          </div>

			          <div class="item">
			            <img src="images/<?php echo $row['evidence1']; ?>" style="width:100%;">
			            <div class="carousel-caption">
			              
			              
			            </div>
			          </div>
			        
			          <div class="item">
			            <img src="images/<?php echo $row['evidence2']; ?>" style="width:100%;">
			            <div class="carousel-caption">
			              
			            </div>
			          </div>
			          <div class="item">
			            <img src="images/<?php echo $row['evidence3']; ?>" style="width:100%;">
			            <div class="carousel-caption">
			              
			            </div>
			          </div>
			          <div class="item">
			            <img src="images/<?php echo $row['evidence4']; ?>" style="width:100%;">
			            <div class="carousel-caption">
			              
			            </div>
			          </div>
			          <div class="item">
			            <img src="images/<?php echo $row['evidence5']; ?>" style="width:100%;">
			            <div class="carousel-caption">
			              
			            </div>
			          </div>
			  </div>

			  <!-- Left and right controls -->
			  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left"></span>
			    
			  </a>
			  <a class="right carousel-control" href="#myCarousel" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right"></span>
			    
			  </a>
			</div>

			<img src="images/<?php echo $row['image']; ?>" alt="" class="img-responsive" style="height:350px; width:70%; margin:auto;">
			<hr>
			<tr>
				<th>Title</th>
				<td><?php echo $row['title']; ?></td>
			</tr>
			
			<tr>
				<th>Category</th>
				<td><?php echo $row['category']; ?></td>
			</tr>
			<tr>
				<th>Skills Used</th>
				<td><?php echo $row['skills_used']; ?></td>
			</tr>
			<tr>
				<th>Description</th>
				<td><?php echo $row['description']; ?></td>
			</tr>
			<tr>
				<th>Notice!!</th>
				<td>
					<div class="alert alert-info">
  					<strong>INFO!</strong> Evidence of the website (given in desccription) are displayed in an image slider above.
					</div>
				</td>
			</tr>
		</table>
			<a href="<?php echo $row['link']; ?>" style="color:white;"	class="btn btn-primary">Visit Site</a>
			
		</div>
			
		<?php				
	}
}
}
else{
	echo "<script>
	alert('Something went wrong!');
	
	</script>";
}
?>