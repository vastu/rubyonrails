<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Utsav Bhandari</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Utsav Bhandari Portfolio" />

<!-- Portfolio-CSS -->	<link rel="stylesheet" href="css/swipebox.css" type="text/css" media="all">
		
<link href="css/aos.css" rel="stylesheet" type="text/css" media="all" /><!-- //animation effects-css-->

<link rel="stylesheet" href="css/index.css"><!-- skills bars CSS-->

<!-- custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />

<!-- //custom-theme -->
	  
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome-icons -->

<!-- googlefonts -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Cairo:200,300,400,600,700,900&amp;subset=arabic,latin-ext" rel="stylesheet">
<!-- //googlefonts -->

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- banner -->
<div class="banner" id="home">
	<div class="agileinfo-dot">
		<div class="container">
		<!-- header -->
		<div class="header-w3layouts"> 
			<!-- Navigation -->
			<nav class="navbar navbar-default navbar-fixed-top"> 
					<div class="navbar-header page-scroll">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">VASTU</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a class="navbar-brand" href="index.html">VastU</a></h1>
					</div> 
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-right cl-effect-15">
							<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
							<li class="hidden"><a class="page-scroll" href="#page-top"></a>	</li>
							<li><a class="page-scroll scroll" href="#home">Home</a></li>
							<li><a class="page-scroll scroll" href="#about">About</a></li>
							<li><a class="page-scroll scroll" href="#skills">Skills</a></li>
							<li><a class="page-scroll scroll" href="#education">Education & Experience </a></li>
							<li><a class="page-scroll scroll" href="#portfolio">Portfolio</a></li>
							<li><a class="page-scroll scroll" href="#contact">Contact</a></li>
							<li><a class="" href="login.php">Log In</a></li>
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				<!-- /.container -->
			</nav>  
		</div>	
		<!-- //header -->


			<?php 
	              include "connection.php";
	                $sql = "SELECT * FROM about";
	                $result = mysqli_query($conn, $sql);

	                if (mysqli_num_rows($result) > 0) {
	                    // output data of each row
	                    while($row = mysqli_fetch_assoc($result)) {
	                       ?>
					
					 
			<div class="w3_banner_info">

				<div class="w3_banner_info_grid">
					<h2 data-aos="fade-right">Hi, i am </h2>
					<h2 data-aos="fade-right"><?php echo $row['name']; ?></h2>
					<h5 style="color:#FFFFF0;"><?php echo $row['designation']; ?></h5>
					<p style="color:#DCDCDC;"><?php echo $row['cover_about']; ?></p>
					<ul data-aos="slide-up">
						<li><a href="#" class="w3ls_more" data-toggle="modal" data-target="#myModal">Know More</a></li>
						<?php 
							$sql = "SELECT * FROM resume ORDER BY resume_id DESC LIMIT 1";
							$result = mysqli_query($conn, $sql);

							if (mysqli_num_rows($result) > 0) {
							    // output data of each row
							    while($row = mysqli_fetch_assoc($result)) {
							       ?>
						
						<!-- <li><a href="downloads.php?filename=<?php echo $row['resume_file'];?>" class="scroll w3l_contact"><i class="fa fa-download" aria-hidden="true"></i> Download CV</a></li> -->
						
						<li><a style="color:black; margin-top: -3px; background: white; border-radius: 2px;" href="downloads.php?filename=<?php echo $row['resume_file'];?>" class="btn btn-outline-danger" ><i class="fa fa-download"  aria-hidden="true">&nbsp; &nbsp;</i> Download CV</a></li>
						
						 <?php 
						   }
						 }
						 else {
						   echo "The Content is not available.";
						   echo "<script>
						   alert('hello');
						   
						   </script>";
						 }            
						?>
					</td>
					</ul>
				</div>
			</div>
			 <?php 
			   }
			 }
			 else {
			   echo "The Page is empty";
			 }            
			?>


			<div class="thim-click-to-bottom">
				<a href="#about" class="scroll">
					<i class="fa fa-arrows-v" aria-hidden="true"></i>
				</a>
			</div>

		</div>
	</div>
</div>
<!-- banner -->

<!-- bootstrap-modal-pop-up -->
	<!-- modal -->
	<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header"> 
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						<h4 class="modal-title">About Me</h4>
				</div> 
				<?php 
	              include "connection.php";
	                $sql = "SELECT * FROM about";
	                $result = mysqli_query($conn, $sql);

	                if (mysqli_num_rows($result) > 0) {
	                    // output data of each row
	                    while($row = mysqli_fetch_assoc($result)) {
	                       ?>

				<div class="modal-body">
					<div class="modalpad"> 
						<div class="modalpop ">
							<img src="images/<?php echo $row['profile_picture']; ?>"  class="img-responsive" alt=""/>
						</div>
						<div class="about-modal wthree">
							<h3>Hi, i'm <span><?php echo $row['name']; ?></span></h3>
							<h4><?php echo $row['designation']; ?></h4>
							<ul class="address">
								<li>
									<ul class="agileits-address-text ">
										<li><b>D.O.B</b></li>
										<li><?php echo $row['dob']; ?></li>
									</ul>
								</li>
								<li>
									<ul class="agileits-address-text">
										<li><b>PHONE </b></li>
										<li><?php echo $row['phone']; ?></li>
									</ul>
								</li>
								<li>
									<ul class="agileits-address-text">
										<li><b>ADDRESS </b></li>
										<li><?php echo $row['address']; ?></li>
									</ul>
								</li>
								<li>
									<ul class="agileits-address-text">
										<li><b>E-MAIL </b></li>
										<li><a href="vastu.bhandari45@gmail.com.com"> <?php echo $row['email']; ?></a></li>
									</ul>
								</li>
								<li>
									<ul class="agileits-address-text">
										<li><b>WEBSITE </b></li>
										<li><a href="utsavbhandari.com"><?php echo $row['website']; ?></a></li>
									</ul>
								</li>
							</ul> 
						</div> 
						<div class="clearfix"> </div>
				</div>
			</div>
			 <?php 
			   }
			 }
			 else {
			   echo "The Page is empty";
			 }            
			?>
		</div>
	</div>
	</div>
	<!-- //modal -->	
<!-- //bootstrap-modal-pop-up --> 

<!-- about -->
<?php 
  include "connection.php";
    $sql = "SELECT * FROM about";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
           ?>
<div class="about" id="about">
	<h3 data-aos="zoom-in">About me</h3>
	<div class="col-md-6 about-left">
		<h3 data-aos="slide-up">Hello</h3>
		<p><?php echo $row['about_me']; ?></p>
		
		<img src="images/<?php echo $row['signature']; ?>" alt="Utsav Bhandari" />
	</div>
	<div data-aos="flip-right" class="col-md-6 about-right">
		<img style="width:90%; height:440px; border-radius:20px;" src="images/<?php echo $row['profile_picture']; ?>" class="img-responsive" alt="" />
	</div>
	<div class="clearfix"></div>
</div>
 <?php 
   }
 }
 else {
   echo "The Page is empty";
 }            
?>
<!-- //about -->

<!-- skills -->

<div class="skills" id="skills">
	<div class="container">
				<h3 data-aos="zoom-in">Skills</h3>
				
		<div class="skill-grids">
			
			<div class="col-md-6 skill-grids-left">
				<?php 
				  include "connection.php";
				    $sql = "SELECT * FROM skills LIMIT 4";
				    $result = mysqli_query($conn, $sql);

				    if (mysqli_num_rows($result) > 0) {
				        // output data of each row
				        while($row = mysqli_fetch_assoc($result)) {
				           ?>
				<div data-aos="flip-left" class="col-md-6 w3labout-img"> 
				<div class="boxw3-agile"> 
					<img src="images/<?php echo $row['picture']; ?>" style="border-radius:5px;" alt="" class="img-responsive" />
					<div class="agile-caption">
						<h3><?php echo $row['skills_on']; ?></h3>
						<p><?php echo $row['skills_desc']; ?></p>
					</div> 
				</div>
				</div>
				 <?php 
				   }
				 }
				 else {
				   echo "The Content is empty";
				 }            
				?>
				<div class="clearfix"></div>
			</div>
			 
			<div class="col-md-6 skill-grids-right">
			<?php 
			  include "connection.php";
			    $sql = "SELECT * FROM skills";
			    $result = mysqli_query($conn, $sql);

			    if (mysqli_num_rows($result) > 0) {
			        // output data of each row
			        while($row = mysqli_fetch_assoc($result)) {
			           ?>		
			<!-- Skills -->
			<div class="skillbar clearfix " data-percent="<?php echo $row['skill_level']; ?>">
				<div class="skillbar-title" style="background: <?php echo $row['skill_bar_color']; ?>;"><span><?php echo $row['skills_on']; ?></span></div>
				<div class="skillbar-bar" style="background: <?php echo $row['colorback']; ?>; width:<?php echo $row['skill_level'];  ?>"></div>
				<div class="skill-bar-percent"><?php echo $row['skill_level']; ?></div>
			</div> <!-- End Skill Bar -->

			

			
			 <?php 
			   }
			 }
			 else {
			   echo "The Content is empty";
			 }            
			?>
				
			<!-- //Skills -->
			</div>
			<div class="clearfix"></div>
		</div>
		
	</div>
</div>

<!-- //skills -->

<!-- /education -->
 <div class="education" id="education">
	    <div class="col-md-5 education-w3l">
		     <h3 data-aos="zoom-in" class="w3l_head three">My Education</h3>
			  <div class="education-agile-grids">
			  	<?php 
	              include "connection.php";
	                $sql = "SELECT level, start_end_date, course, course_description FROM education";
	                $result = mysqli_query($conn, $sql);

	                if (mysqli_num_rows($result) > 0) {
	                    // output data of each row
	                    while($row = mysqli_fetch_assoc($result)) {
	                       ?>
				  <div class="education-agile-w3l">
				     <div class="education-agile-w3l-year" style="height:240px;">
					       <h4><?php echo $row['start_end_date']; ?></h4>
						   <h6><?php echo $row['level']; ?></h6>
				     </div>
					 <div class="education-agile-w3l-info">
					       <h4><?php echo $row['course']; ?></h4>
						   <p><?php echo $row['course_description']; ?></p>
						  
				     </div>
				      <div class="clearfix"></div>
				  </div>
				   <?php 
				     }
				   }
				   else {
				     echo "The Content is empty";
				   }            
				  ?>
				  <div class="clearfix"></div>
				 
			  </div>
		</div>
		
		<div data-aos="slide-up" class="col-md-2 middle">
			<i class="fa fa-hourglass-end" aria-hidden="true"></i>
		</div>
		
	    <div class="col-md-5 education-w3l">
		     <h3 data-aos="zoom-in" class="w3l_head three">My Experience</h3>
			  <div class="education-agile-grids">
			  			  	<?php 
			  	              include "connection.php";
			  	                $sql = "SELECT start_end_date,workplace,post,description FROM experience";
			  	                $result = mysqli_query($conn, $sql);

			  	                if (mysqli_num_rows($result) > 0) {
			  	                    // output data of each row
			  	                    while($row = mysqli_fetch_assoc($result)) {
			  	                       ?>
				  <div class="education-agile-w3l">
				     <div class="education-agile-w3l-year">
					       <h4><?php echo $row['start_end_date']; ?></h4>
						   <h6><?php echo $row['workplace']; ?></h6>
				     </div>
					 <div class="education-agile-w3l-info">
					       <h4><?php echo $row['post']; ?></h4>
						   <p><?php echo $row['description']; ?></p>
						  
				     </div>
				      <div class="clearfix"></div>
				  </div>
				   <?php 
				     }
				   }
				   else {
				     echo "The Content is empty";
				   }            
				  ?>
				  <div class="clearfix"></div>

				  
				 
			  </div>
		</div>
		 <div class="clearfix"> </div>
		</div>
 <!-- //education -->
 
 <!-- Portfolio -->
	<div class="portfolio" id="portfolio">
		<h3 data-aos="zoom-in" >Portfolio</h3>

		<div class="tabs tabs-style-bar">
			<nav>
				<ul>
					<li><a href="#section-bar-1" class="icon icon-box"><span>Web Development</span></a></li>
					<li><a href="#section-bar-2" class="icon icon-display"><span>Software Development</span></a></li>
					
				</ul>
			</nav>

			<div class="content-wrap">

				<!-- Tab-1 -->
				<section id="section-bar-1" class="agileits w3layouts">
					<h4>Web Development</h4>
					<div class="gallery-grids">
								  	<?php 
						              include "connection.php";
						                $sql = "SELECT * FROM portfolio where category='web' ";
						                $result = mysqli_query($conn, $sql);

						                if (mysqli_num_rows($result) > 0) {
						                    // output data of each row
						                    while($row = mysqli_fetch_assoc($result)) {
						                       ?>

						<div class="col-md-4 col-sm-4 gallery-top">
									  	
								<figure class="effect-bubba">
									<img src="images/<?php echo $row['image']; ?>" alt="" style="height:250px; width:400px;" class="img-responsive">
									<figcaption>
										<h4><?php echo $row['tag']; ?> </h4>

										<button data-toggle="modal" data-target="#view-modal" data-id="<?php echo $row['portfolio_id']; ?>" id="show_more" class="btn btn-sm btn-default"> <i class="glyphicon glyphicon-eye-open" style="margin-right: 8px;"></i>More</button>
									</figcaption>
								</figure>	
								 					
						</div>
						

						 <?php 
						   }
						 }
						 else {
						   echo "The Content is empty";
						 }            
						?>	
						<div class="clearfix"></div>
					</div>
				</section>
				

			<!-- Modal -->
				 <div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; ">
				      <div class="modal-dialog" style="width:60%;"> 
				           <div class="modal-content"> 
				           
				                <div class="modal-header"> 
				                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
				                     <h4 class="modal-title">
				                     	<i class="glyphicon glyphicon-file"></i> Portfolio
				                     </h4> 
				                </div> 
				                <div class="modal-body"> 
				                
				                	   <div id="modal-loader" style="display: none; text-align: center;">
				                	   	<img src="ajax-loader.gif">
				                	   </div>
				                     
				                    <!-- content will be load here -->                          
				                    <div id="dynamic-content"></div>
				                      
				                 </div> 
				                 <div class="modal-footer"> 
				                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
				                 </div> 
				                 
				          </div> 
				       </div>
				</div><!-- /.modal -->   

				<!-- //Tab-1 -->

				<!-- Tab-2 -->
				<section id="section-bar-2" class="agileits w3layouts">
					<h4>Softwares</h4>
					<div class="gallery-grids">
								  	<?php 
						              include "connection.php";
						                $sql = "SELECT * FROM portfolio where category='software' ";
						                $result = mysqli_query($conn, $sql);

						                if (mysqli_num_rows($result) > 0) {
						                    // output data of each row
						                    while($row = mysqli_fetch_assoc($result)) {
						                       ?>
						<div class="col-md-4 col-sm-4 gallery-top">
									  	
								<figure class="effect-bubba">
									<img src="images/<?php echo $row['image']; ?>" alt="" class="img-responsive">
									<figcaption>
										<h4><?php echo $row['tag']; ?> </h4>

										<button data-toggle="modal" data-target="#view-modal" data-id="<?php echo $row['portfolio_id']; ?>" id="show_more" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-eye-open"></i> Know More</button>
									</figcaption>
								</figure>	
								 					
						</div>

						 <?php 
						   }
						 }
						 else {
						   echo "The Content is empty";
						 }            
						?>	
						<div class="clearfix"></div>
					</div>
				</section>
			
				
			</div><!-- //Content -->
		</div><!-- //Tabs -->
</div>
<!-- //Portfolio -->


<?php 
if(isset($_POST['submit'])){
    include 'connection.php';

        // $name=mysqli_real_escape_string($conn,$_POST['fullname']);
    $name=mysqli_real_escape_string($conn,$_POST['name']);
    $email=mysqli_real_escape_string($conn,$_POST['email']);
    $company=mysqli_real_escape_string($conn,$_POST['company']);
    $phone=mysqli_real_escape_string($conn,$_POST['phone']);
    $message=mysqli_real_escape_string($conn,$_POST['message']);
    

            // $query="UPDATE about
            // SET skills_on ='$skills_on', skills_desc='$skills_description', skill_level='$skill_level', picture='$location'
            // WHERE name = 'utsav' ";

        $query="INSERT INTO messages (name,email,company_name,phone,message ) VALUES('$name','$email','$company','$phone','$message')";
        if(mysqli_query($conn,$query)){
           echo '<script>alert("Congrats the Data has successfully been inserted!")</script>';
           echo '<script>window.location.href="index.php"</script>'; 
       }
       else{
           echo '<script>alert("Sorry, failed to upload the data !!!")</script>';
           echo"Sorry, failed to upload the data !!! ";
       }
   }

   ?>
<!-- contact -->
<div class="contact" id="contact">
			  	<?php 
	              include "connection.php";
	                $sql = "SELECT * FROM contacts";
	                $result = mysqli_query($conn, $sql);

	                if (mysqli_num_rows($result) > 0) {
	                    // output data of each row
	                    while($row = mysqli_fetch_assoc($result)) {
	                       ?>
	<div class="container">
		<div class="col-md-6 contact-left">
			<h3 data-aos="zoom-in" >Contact me</h3>
				<p><?php echo $row['description']; ?></p>
			<form action="" method="post" enctype="multipart/form-data">
					<div class="col-md-6 agileits_agile_about_mail_left">
						<input type="text" name="name" placeholder="Name" required="">
						<input type="text" name="company" placeholder="Company Name" required="">
					</div>
					<div class="col-md-6 agileits_agile_about_mail_left">
						<input type="email" name="email" placeholder="Email" required="">
						<input type="text" name="phone" placeholder="Phone" required="">
					</div>
				<div class="clearfix"> </div>
				<textarea name="message" placeholder="Message..." required=""></textarea>
				<input type="submit" name="submit" value="Submit">
			</form>
		</div>
		<div class="col-md-6 contact-right">
			<div data-aos="flip-down" class="col-md-6 contactright1">
			<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
			<h4>Location</h4>
			<p><?php echo $row['address']; ?></p>
			</div>
			<div data-aos="flip-down" class="col-md-6 contactright1">
			<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
			<h4>Phone</h4>
			<p><?php echo $row['phone1']; ?></p>
			<p><?php echo $row['phone2']; ?></p>
			</div>
			<div class="clearfix"></div>
			<div data-aos="flip-up"class="col-md-6 contactright1">
			<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
			<h4>Support</h4>
			<p><a href="mailto:info@example.com"><?php echo $row['support']; ?></a></p>
			</div>
			<div data-aos="flip-up" class="col-md-6 contactright1">
			<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
			<h4>Mail</h4>
			<p><a href="mailto:info@example.com"><?php echo $row['email']; ?></a></p>
			<p><a href="mailto:info@example.com"><?php echo $row['email']; ?></a></p>
			</div>
			<div class="clearfix"></div>
			<ul class="top-links">
				<li data-aos="flip-right"><a href="<?php echo $row['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
				<li data-aos="flip-right"><a href="<?php echo $row['description']; ?>"><i class="fa fa-instagram"></i></a></li>
				<li data-aos="flip-right"><a href="<?php echo $row['linkedin']; ?>"><i class="fa fa-linkedin"></i></a></li>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>

	   <?php 
	     }
	   }
	   else {
	     echo "The Content is empty";
	   }            
	  ?>
</div>
<!-- //contact -->

<!-- map -->
<div class="map">
	<h3 data-aos="zoom-in" >Locate Me</h3>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28268.72778897573!2d85.25740707068314!3d27.66812394833863!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1812d78377ef%3A0x8c37cded908543b!2sKirtipur+44600!5e0!3m2!1sen!2snp!4v1519545766531" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- //map -->

<!-- copyright -->
<div class="copyright-agile">
	<div class="container">
		<h4> Vastu</h4>
		<p>© 2017 Vastu. All rights reserved | Design by Utsav Bhandari</p>
		<div class="clearfix"></div>
	</div>
</div>
<!-- copyright -->

<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- //js -->

<!-- Gallery-Tab-JavaScript -->
			<script src="js/cbpFWTabs.js"></script>
			<script>
				(function() {
					[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
						new CBPFWTabs( el );
					});
				})();
			</script>
<!-- //Gallery-Tab-JavaScript -->
	<!-- Swipe-Box-JavaScript -->
			<script src="js/jquery.swipebox.min.js"></script> 
				<script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
			</script>
		<!-- //Swipe-Box-JavaScript -->

<!-- Scrolling Nav JavaScript --> 
    <script src="js/scrolling-nav.js"></script>  
<!-- //fixed-scroll-nav-js --> 

<script src="js/index.js"></script><!-- skills bars JS FILE-->
	
<!-- animation effects-js files-->
	<script src="js/aos.js"></script><!-- //animation effects-js-->
	<script src="js/aos1.js"></script><!-- //animation effects-js-->
<!-- animation effects-js files-->

<!-- //here starts scrolling icon -->
<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<!-- here stars scrolling script -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>

	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

	<!-- //here ends scrolling script -->
<!-- //here ends scrolling icon -->

<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!-- //scrolling script -->

<script>
$(document).ready(function(){
	
	$(document).on('click', '#show_more', function(e){
		
		e.preventDefault();
		
		var uid = $(this).data('id');   // it will get id of clicked row
		
		$('#dynamic-content').html(''); // leave it blank before ajax call
		$('#modal-loader').show();      // load ajax loader
		
		$.ajax({
			url: 'show_more.php',
			type: 'POST',
			data: 'id='+uid,
			dataType: 'html'
		})
		.done(function(data){
			console.log(data);	
			$('#dynamic-content').html('');    
			$('#dynamic-content').html(data); // load response 
			$('#modal-loader').hide();		  // hide ajax loader	
		})
		.fail(function(){
			$('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#modal-loader').hide();
		});
		
	});
	
});

</script>

</body>
</html>