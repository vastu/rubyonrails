<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Utsav Bhandari</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

    <div class="wrapper">
        <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

 <div class="sidebar-wrapper">
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text">
            Utsav Bhandari
        </a>
    </div>

    <ul class="nav">
        <li class="active">
            <a href="dashboard.php">
                <i class="ti-panel"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li>
            <a href="messages.php">
                <i class="ti-comments"></i>
                <p>Messages</p>
            </a>
        </li>
        <li>
            <a href="user.php">
                <i class="ti-user"></i>
                <p>User Profile</p>
            </a>
        </li>
        <li>
            <a href="skills.php">
                <i class="ti-pencil-alt2"></i>
                <p>Skills</p>
            </a>
        </li>
        <li>
            <a href="education.php">
                <i class="ti-book"></i>
                <p>Education</p>
            </a>
        </li>
        <li>
            <a href="experience.php">
                <i class="ti-desktop"></i>
                <p>Experience</p>
            </a>
        </li>
        <li>
            <a href="portfolio.php">
                <i class="ti-id-badge"></i>
                <p>Add Portfolio</p>
            </a>
        </li>
        <li>
            <a href="contacts.php">
                <i class="ti-location-pin"></i>
                <p>Contact Details</p>
            </a>
        </li>
        
       
        <li>
            <a href="about_details.php">
                <i class="ti-bell"></i>
                <p>About Details</p>
            </a>
        </li>

        
    </ul>
</div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand" href="#">Dashboard</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="ti-panel"></i>
                            <p>Stats</p>
                        </a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="ti-bell"></i>
                        <p class="notification">5</p>
                        <p>Notifications</p>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Notification 1</a></li>
                        <li><a href="#">Notification 2</a></li>
                        <li><a href="#">Notification 3</a></li>
                        <li><a href="#">Notification 4</a></li>
                        <li><a href="#">Another notification</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="ti-settings"></i>
                        <p>Settings</p>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</nav>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-server"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Capacity</p>
                                    105GB
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Revenue</p>
                                    $1,345
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-calendar"></i> Last day
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-pulse"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Errors</p>
                                    23
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-timer"></i> In the last hour
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-twitter-alt"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Followers</p>
                                    +45
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <hr>
       

        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class=" col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Contact Details</h4>
                            </div>

                            <?php 
                            if(isset($_POST['update_contacts'])){
                                include '../connection.php';

                                    
                                $location=mysqli_real_escape_string($conn,$_POST['location']);
                                $phone1=mysqli_real_escape_string($conn,$_POST['phone1']);
                                $phone2=mysqli_real_escape_string($conn,$_POST['phone2']);
                                $support=mysqli_real_escape_string($conn,$_POST['support']);
                                $description=mysqli_real_escape_string($conn,$_POST['description']); 
                                $email=mysqli_real_escape_string($conn,$_POST['email']);
                                $facebook=mysqli_real_escape_string($conn,$_POST['facebook']);
                                $linkedin=mysqli_real_escape_string($conn,$_POST['linkedin']);
                                $instagram=mysqli_real_escape_string($conn,$_POST['instagram']);


                                        $query="UPDATE contacts SET address ='$location', phone1='$phone1', phone2='$phone2', support='$support', description='$description', email='$email', facebook='$facebook',linkedin='$linkedin',instagram='$instagram' where email='$email' ";

                                    // $query="INSERT INTO skills (skills_on,skills_desc,skill_workplace,picture ) VALUES('$skills_on','$skills_description','$skill_workplace','$location')";
                                    if(mysqli_query($conn,$query)){
                                       echo '<script>alert("Congrats the Data has successfully been updated!")</script>';
                                       echo '<script>window.location.href="contacts.php"</script>'; 
                                   }
                                   else{
                                       echo '<script>alert("Sorry, failed to update the data !!!")</script>';
                                       echo"Sorry, failed to upload the data !!! ";
                                   }
                               }

                               ?>


                                    <div class="content">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label>Location</label>
                                                        <input type="text" name="location" class="form-control border-input" placeholder="Location" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label>Phone 1</label>
                                                        <input type="text" name="phone1" class="form-control border-input" placeholder="Phone 1" value="">
                                                    </div>
                                                </div>

                                            </div>



                                            <div class="row">                                        
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Phone 2</label>
                                                        <input type="text" name="phone2" class="form-control border-input" placeholder="Phone 2" value="">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Support</label>
                                                            <input type="text" name="support" class="form-control border-input" placeholder="Support" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>email</label>
                                                        <input type="text" name="email" class="form-control border-input" placeholder="Email" value="">
                                                    </div>
                                                    </div>
                                                
                                            </div>

                                            <div class="row">                                        
                                                
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Facebook Link</label>
                                                            <input type="text" name="facebook" class="form-control border-input" placeholder="Social Media" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>LinkedIn Link</label>
                                                            <input type="text" name="linkedin" class="form-control border-input" placeholder="Social Media" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Instagram Link</label>
                                                            <input type="text" name="instagram" class="form-control border-input" placeholder="Social Media" value="">
                                                        </div>
                                                    </div>
                                                
                                            </div>

                                            <div class="row">
                                                
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea rows="" name="description" class="form-control border-input" placeholder="Here can be your description" value="Here can be your description...">
                                                            
                                                        </textarea>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="text-center">
                                                <button type="submit" name="update_contacts" class="btn btn-primary btn-fill btn-wd">Update Contact details</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                     </div>
                 </div>
             </div>

             <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>

                            <li>
                                <a href="http://www.utsavbhandari.com">
                                    Utsav Bhandari
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                   Portfilio
                               </a>
                           </li>
                           <li>
                            <a href="">
                                Licenses
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Utsav Bhandari</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="assets/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<script type="text/javascript">
   $(document).ready(function(){

     demo.initChartist();

     $.notify({
         icon: 'ti-gift',
         message: "Welcome to <b>Paper Dashboard</b> - a beautiful Bootstrap freebie for your next project."

     },{
        type: 'success',
        timer: 4000
    });

 });
</script>

</html>
