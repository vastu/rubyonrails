<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Utsav Bhandari</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

    <div class="wrapper">
        <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

 <div class="sidebar-wrapper">
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text">
            Utsav Bhandari
        </a>
    </div>

    <ul class="nav">
        <li class="active">
            <a href="dashboard.php">
                <i class="ti-panel"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li>
            <a href="messages.php">
                <i class="ti-comments"></i>
                <p>Messages</p>
            </a>
        </li>
        <li>
            <a href="user.php">
                <i class="ti-user"></i>
                <p>User Profile</p>
            </a>
        </li>
        <li>
            <a href="skills.php">
                <i class="ti-pencil-alt2"></i>
                <p>Skills</p>
            </a>
        </li>
        <li>
            <a href="education.php">
                <i class="ti-book"></i>
                <p>Education</p>
            </a>
        </li>
        <li>
            <a href="experience.php">
                <i class="ti-desktop"></i>
                <p>Experience</p>
            </a>
        </li>
        <li>
            <a href="portfolio.php">
                <i class="ti-id-badge"></i>
                <p>Add Portfolio</p>
            </a>
        </li>
        <li>
            <a href="contacts.php">
                <i class="ti-location-pin"></i>
                <p>Contact Details</p>
            </a>
        </li>

        <li>
            <a href="about_details.php">
                <i class="ti-bell"></i>
                <p>About Details</p>
            </a>
        </li>


    </ul>
</div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand" href="#">Dashboard</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="ti-panel"></i>
                            <p>Stats</p>
                        </a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="ti-bell"></i>
                        <p class="notification">5</p>
                        <p>Notifications</p>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Notification 1</a></li>
                        <li><a href="#">Notification 2</a></li>
                        <li><a href="#">Notification 3</a></li>
                        <li><a href="#">Notification 4</a></li>
                        <li><a href="#">Another notification</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="ti-settings"></i>
                        <p>Settings</p>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</nav>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-server"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Capacity</p>
                                    105GB
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Revenue</p>
                                    $1,345
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-calendar"></i> Last day
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-pulse"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Errors</p>
                                    23
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-timer"></i> In the last hour
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-twitter-alt"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Followers</p>
                                    +45
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="stats">
                                <i class="ti-reload"></i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="header">
                    <h4 class="title">Portfolio View</h4>
                       <p class="category">The details are ordered by the date received</p>
                   </div>
                   <div class="content table-responsive table-full-width">
                    <div class="table-responsive">

                     <table class="table table-responsive table-bordered " style="overflow-y: auto; height:400px; width: 100%; display: block">

                       <thead>
                         <tr>
                           <th>Portfolio Id</th>
                           <th>Title</th>
                           <th>Description</th>
                           <th>Tag</th>                             
                           <th>Category</th>
                           <th>Image</th>                              
                           <th>Delete</th>
                       </tr>
                   </thead>
                   <tbody>
                     <?php 
                     include "../connection.php";
                     $sql = "SELECT * FROM portfolio order by portfolio_id DESC";
                     $result = mysqli_query($conn, $sql);

                     if (mysqli_num_rows($result) > 0) {
                                        // output data of each row
                        while($row = mysqli_fetch_assoc($result)) {
                         ?>
                         <tr>
                           <td><?php echo $row['portfolio_id']; ?></td>
                           <td><?php echo $row['title']; ?></td>
                           <td><?php echo $row['description']; ?></td>
                           <td><?php echo $row['tag']; ?></td>
                           <td><?php echo $row['category']; ?></td>
                           <td><?php echo $row['image']; ?></td>
                           <td><a href="delete.php?action_port=delete_port&id=<?php echo $row["portfolio_id"]; ?>" class="btn btn-danger btn-block">Delete </a></td>
                       </tr>
                       <?php 

                   }
               }

               ?>
           </tbody>

       </table>

   </div>
</div>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-12">
       <div class="card">
           <div class="header">
               <h4 class="title">Skills View</h4>
               <p class="category">The details are ordered by the date received</p>
           </div>
           <div class="content table-responsive table-full-width">
            <div class="table-responsive">

             <table class="table table-responsive table-bordered " style="overflow-y: auto; height:400px; width: 100%; display: block">

               <thead>
                 <tr>
                   <th>Skills Id</th>
                   <th>Skill Name</th>
                   <th>Description</th>
                   <th>Skill Level</th>                             
                   <th>Skill Bar Color</th> 
                   <th>Picture</th>                            
                   <th>Delete</th>
               </tr>
           </thead>
           <tbody>
             <?php 
             include "../connection.php";
             $sql = "SELECT * FROM skills order by skill_id DESC";
             $result = mysqli_query($conn, $sql);

             if (mysqli_num_rows($result) > 0) {
                                    // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                 ?>
                 <tr>
                   <td><?php echo $row['skill_id']; ?></td>
                   <td><?php echo $row['skills_on']; ?></td>
                   <td><?php echo $row['skills_desc']; ?></td>
                   <td><?php echo $row['skill_level']; ?></td>
                   <td><?php echo $row['skill_bar_color']; ?></td>
                   <td><?php echo $row['picture']; ?></td>

                   <td><a href="delete.php?action_skill=delete_skill&id=<?php echo $row["skill_id"]; ?>" class="btn btn-danger btn-block">Delete </a></td>
               </tr>
               <?php 

           }
       }

       ?>
   </tbody>

</table>

</div>
</div>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-12">
       <div class="card">
           <div class="header">
               <h4 class="title">Education View</h4>
               <p class="category">The details are ordered by the date received</p>
           </div>
           <div class="content table-responsive table-full-width">
            <div class="table-responsive">

             <table class="table table-responsive table-bordered " style="overflow-y: auto; height:400px; width: 100%; display: block">

               <thead>
                 <tr>
                   <th>Education Id</th>
                   <th>Level</th>
                   <th>Start End Date</th>
                   <th>Course</th>                             
                   <th>Course Description</th>                             
                   <th>Delete</th>
               </tr>
           </thead>
           <tbody>
             <?php 
             include "../connection.php";
             $sql = "SELECT * FROM education order by education_id DESC";
             $result = mysqli_query($conn, $sql);

             if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                 ?>
                 <tr>
                   <td><?php echo $row['education_id']; ?></td>
                   <td><?php echo $row['level']; ?></td>
                   <td><?php echo $row['start_end_date']; ?></td>
                   <td><?php echo $row['course']; ?></td>

                   <td><?php echo $row['course_description']; ?></td>

                   <td><a href="delete.php?action_edu=delete_edu&id=<?php echo $row["education_id"]; ?>" class="btn btn-danger btn-block">Delete </a></td>
               </tr>
               <?php 

           }
       }

       ?>
   </tbody>

</table>

</div>
</div>
</div>
</div>
</div>


<div class="row">
   <div class="col-md-12">
       <div class="card">
           <div class="header">
               <h4 class="title">Experience View</h4>
               <p class="category">The details are ordered by the date received</p>
           </div>
           <div class="content table-responsive table-full-width">
            <div class="table-responsive">

             <table class="table table-responsive table-bordered " style="overflow-y: auto; height:400px; width: 100%; display: block">

               <thead>
                 <tr>
                   <th>Experience Id</th>
                   <th>Workplace</th>
                   <th>Start End Date</th>
                   <th>Post</th>                             
                   <th>Description</th>                             
                   <th>Delete</th>
               </tr>
           </thead>
           <tbody>
             <?php 
             include "../connection.php";
             $sql = "SELECT * FROM experience order by experience_id DESC";
             $result = mysqli_query($conn, $sql);

             if (mysqli_num_rows($result) > 0) {
                                    // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                 ?>
                 <tr>
                   <td><?php echo $row['experience_id']; ?></td>
                   <td><?php echo $row['workplace']; ?></td>
                   <td><?php echo $row['start_end_date']; ?></td>
                   <td><?php echo $row['post']; ?></td>

                   <td><?php echo $row['description']; ?></td>

                   <td><a href="delete.php?action_exp=delete_exp&id=<?php echo $row["experience_id"]; ?>" class="btn btn-danger btn-block">Delete </a></td>
               </tr>
               <?php 

           }
       }

       ?>
   </tbody>

</table>

</div>
</div>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-12">
       <div class="card">
           <div class="header">
               <h4 class="title">Account View</h4>
               <p class="category">The details are ordered by the date received</p>
           </div>
           <div class="content table-responsive table-full-width">
            <div class="table-responsive">

             <table class="table table-responsive table-bordered " style="overflow-y: auto; max-height:300px; width: 100%; display: block">

               <thead>
                 <tr>
                   <th>Account Id</th>
                   <th>Name</th>
                   <th>Email</th>
                   <th>Password</th>                                                          
                   <th>Delete</th>
               </tr>
           </thead>
           <tbody>
             <?php 
             include "../connection.php";
             $sql = "SELECT * FROM account order by account_id DESC";
             $result = mysqli_query($conn, $sql);

             if (mysqli_num_rows($result) > 0) {
                                    // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                 ?>
                 <tr>
                   <td><?php echo $row['account_id']; ?></td>
                   <td><?php echo $row['name']; ?></td>
                   <td><?php echo $row['email']; ?></td>
                   <td><?php echo $row['password']; ?></td>                                                                                
                   <td><a href="delete.php?action_acc=delete_acc&id=<?php echo $row["account_id"]; ?>" class="btn btn-danger btn-block">Delete </a></td>
               </tr>
               <?php 

           }
       }

       ?>
   </tbody>

</table>

</div>
</div>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-12">
       <div class="card">
           <div class="header">
               <h4 class="title">Contact View</h4>
               <p class="category">The details are ordered by the date received</p>
           </div>
           <div class="content table-responsive table-full-width">
            <div class="table-responsive">

             <table class="table table-responsive table-bordered " style="overflow-y: auto; max-height:400px; width: 100%; display: block">

               <thead>
                 <tr>
                   <th>Description</th>
                   <th>Address</th>
                   <th>Phone 1</th>
                   <th>Phone 2</th>                             
                   <th>Email</th>                             
                   <th>Support</th>
                   <th>Facebook</th>                             
                   <th>Instagram</th>                             
                   <th>LinkedIn</th>
               </tr>
           </thead>
           <tbody>
             <?php 
             include "../connection.php";
             $sql = "SELECT * FROM contacts";
             $result = mysqli_query($conn, $sql);

             if (mysqli_num_rows($result) > 0) {
                                    // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                 ?>
                 <tr>
                   <td><?php echo $row['description']; ?></td>
                   <td><?php echo $row['address']; ?></td>
                   <td><?php echo $row['phone1']; ?></td>
                   <td><?php echo $row['phone2']; ?></td>                                 
                   <td><?php echo $row['email']; ?></td>
                   <td><?php echo $row['support']; ?></td>
                   <td><?php echo $row['facebook']; ?></td>
                   <td><?php echo $row['instagram']; ?></td>                                 
                   <td><?php echo $row['linkedin']; ?></td>

               </tr>
               <?php 

           }
       }

       ?>
   </tbody>

</table>

</div>
</div>
</div>
</div>
</div>







</div>


<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>

                <li>
                    <a href="http://www.utsavbhandari.com">
                        Utsav Bhandari
                    </a>
                </li>
                <li>
                    <a href="#">
                       Portfilio
                   </a>
               </li>
               <li>
                <a href="">
                    Licenses
                </a>
            </li>
        </ul>
    </nav>
    <div class="copyright pull-right">
        &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Utsav Bhandari</a>
    </div>
</div>
</footer>

</div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="assets/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<script type="text/javascript">
 $(document).ready(function(){

     demo.initChartist();

     $.notify({
         icon: 'ti-gift',
         message: "Welcome to <b>Paper Dashboard</b> - a beautiful Bootstrap freebie for your next project."

     },{
        type: 'success',
        timer: 4000
    });

 });
</script>

</html>
