<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Utsav Bhandari</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Utsav Bhandari
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="dashboard.php">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="messages.php">
                        <i class="ti-comments"></i>
                        <p>Messages</p>
                    </a>
                </li>
                <li class="active">
                    <a href="user.html">
                        <i class="ti-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li>
                    <a href="skills.php">
                        <i class="ti-pencil-alt2"></i>
                        <p>Skills</p>
                    </a>
                </li>
                <li>
                    <a href="education.php">
                        <i class="ti-book"></i>
                        <p>Education</p>
                    </a>
                </li>
                <li>
                    <a href="experience.php">
                        <i class="ti-desktop"></i>
                        <p>Experience</p>
                    </a>
                </li>
                <li>
                    <a href="portfolio.php">
                        <i class="ti-id-badge"></i>
                        <p>Add Portfolio</p>
                    </a>
                </li>
                <li>
                    <a href="contacts.php">
                        <i class="ti-location-pin"></i>
                        <p>Contact Details</p>
                    </a>
                </li>
                <li>
                    <a href="table.html">
                        <i class="ti-view-list-alt"></i>
                        <p>Table List</p>
                    </a>
                </li>
                <li>
                    <a href="typography.html">
                        <i class="ti-text"></i>
                        <p>Typography</p>
                    </a>
                </li>
                <li>
                    <a href="skills.php">
                        <i class="ti-pencil-alt2"></i>
                        <p>Skills</p>
                    </a>
                </li>
                <li>
                    <a href="maps.html">
                        <i class="ti-map"></i>
                        <p>Maps</p>
                    </a>
                </li>
                <li>
                    <a href="notifications.html">
                        <i class="ti-bell"></i>
                        <p>Notifications</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="upgrade.html">
                        <i class="ti-export"></i>
                        <p>Upgrade to PRO</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">User Profile</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-panel"></i>
								<p>Stats</p>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-bell"></i>
                                    <p class="notification">5</p>
									<p>Notifications</p>
									<b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
						<li>
                            <a href="#">
								<i class="ti-settings"></i>
								<p>Settings</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class=" col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profile</h4>
                            </div>

                            <?php 
                              if(isset($_POST['update_profile_info'])){
                                include '../connection.php';
                                        
                                    // $name=mysqli_real_escape_string($conn,$_POST['fullname']);
                                    $designation=mysqli_real_escape_string($conn,$_POST['designation']);
                                    $dob=mysqli_real_escape_string($conn,$_POST['dob']);
                                    $phone=mysqli_real_escape_string($conn,$_POST['phone']);
                                    $address=mysqli_real_escape_string($conn,$_POST['address']);
                                    $email=mysqli_real_escape_string($conn,$_POST['email']);
                                    $website=mysqli_real_escape_string($conn,$_POST['website']);
                                    $about=mysqli_real_escape_string($conn,$_POST['about']);
                                    $cover_about=mysqli_real_escape_string($conn,$_POST['cover_about']);
                                    $profile=$_FILES['profile_picture']['tmp_name'];
                                    $image= addslashes(file_get_contents($_FILES['profile_picture']['tmp_name']));
                                    $image_name= addslashes($_FILES['profile_picture']['name']);
                                    move_uploaded_file($_FILES["profile_picture"]["tmp_name"],"../images/" . $_FILES["profile_picture"]["name"]); // moves uploaded file inside the images folder.                                           
                                    $profilelocation="../images/" . $_FILES["profile_picture"]["name"];  

                                    $cover=$_FILES['cover_picture']['tmp_name'];
                                    $image= addslashes(file_get_contents($_FILES['cover_picture']['tmp_name']));
                                    $image_name= addslashes($_FILES['profile_picture']['name']);
                                    move_uploaded_file($_FILES["cover_picture"]["tmp_name"],"../images/" . $_FILES["cover_picture"]["name"]); // moves uploaded file inside the images folder.                                           
                                    $coverlocation="../images/" . $_FILES["cover_picture"]["name"]; 

                                    $signature=$_FILES['signature']['tmp_name'];
                                    $image= addslashes(file_get_contents($_FILES['signature']['tmp_name']));
                                    $image_name= addslashes($_FILES['signature']['name']);
                                    move_uploaded_file($_FILES["signature"]["tmp_name"],"../images/" . $_FILES["signature"]["name"]); // moves uploaded file inside the images folder.                                           
                                    $signaturelocation="../images/" . $_FILES["signature"]["name"];   
                                        
                                        $query="UPDATE about
                                        SET designation ='$designation', dob='$dob', phone='$phone', address='$address', email='$email', website= '$website', profile_picture= '$profilelocation', cover_picture= '$coverlocation', about_me= '$about', signature_image= '$signaturelocation', cover_about='$cover_about'
                                        WHERE name = 'utsav' ";

                                        // $query="INSERT INTO about (name,designation,dob,phone,address,email,website,profile_picture,cover_picture,about_me,signature_image ) VALUES('$name','$designation','$dob','$phone','$address','$email','$website','$profilelocation','$coverlocation','$about','$signaturelocation')";
                                        if(mysqli_query($conn,$query)){
                                           echo '<script>alert("Congrats the Data has successfully been updated!")</script>';
                                           echo '<script>window.location.href="user.php"</script>'; 
                                        }
                                        else{
                                           echo '<script>alert("Sorry, failed to upload the data !!!")</script>';
                                            echo"Sorry, failed to upload the data !!! ";
                                        }
                                }
                                
                            ?>

                            <?php 
                                  include "../connection.php";
                                    $sql = "SELECT * FROM about";
                                    $result = mysqli_query($conn, $sql);

                                    if (mysqli_num_rows($result) > 0) {
                                        // output data of each row
                                        while($row = mysqli_fetch_assoc($result)) {
                                           ?>

                            <div class="content">
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" name="fullname" class="form-control border-input" disabled name="name" placeholder="Company" value="Utsav Bhandari">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <input type="text" name="designation" class="form-control border-input" placeholder="Designation" value="<?php echo $row['designation']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Date of Birth</label>
                                                <input type="date" name="dob" class="form-control border-input" placeholder="DOB" value="<?php echo $row['dob']; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input type="text" name="phone" class="form-control border-input" placeholder="Phone Number" value="<?php echo $row['phone']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" name="email" class="form-control border-input" value="<?php echo $row['email']; ?>" placeholder="Email">
                                            </div>                                        
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" name="address" class="form-control border-input" placeholder="Home Address" value="<?php echo $row['address']; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Website</label>
                                                <input type="text" name="website" class="form-control border-input" placeholder="Website" value="<?php echo $row['website']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Profile Picture</label>
                                                <input type="file" name="profile_picture" class="form-control border-input" placeholder="Profile Picture" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Cover Picture</label>
                                                <input type="file" name="cover_picture" class="form-control border-input" placeholder="Cover Picture">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Signature</label>
                                                <input type="file" name="signature" class="form-control border-input" placeholder="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About Me For Cover</label>
                                                <textarea rows="" name="cover_about" class="form-control border-input" placeholder="Here can be your description" value="">
                                                    <?php echo $row['cover_about']; ?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About Me</label>
                                                <textarea rows="" name="about" class="form-control border-input" placeholder="Here can be your description" value="">
                                                    <?php echo $row['about_me']; ?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" name="update_profile_info" class="btn btn-info btn-fill btn-wd">Update Profile</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>

                             <?php 
                               }
                             }
                             else {
                               echo "No data in the form..";
                             }            
                            ?>

                        </div>
                    </div>


                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="http://www.creative-tim.com">
                                Utsav Bhandari
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                               Blog
                            </a>
                        </li>
                        <li>
                            <a href="http://www.creative-tim.com/license">
                                Licenses
                            </a>
                        </li>
                    </ul>
                </nav>
				<div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Utsav Bhandari</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

</html>
